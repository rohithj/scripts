#!/bin/sh

# Rohith Jagannathan
# Install youtube-dl from https://github.com/rg3/youtube-dl first.
# Usage:
# youtubeDl.sh <youtube-url> <path-to-save-file>
# <path-to-save-file> should contain filename

if [ "$1" != "" ]; then
    if [ "$2" != "" ]; then
        youtube-dl --extract-audio --audio-format mp3 "$1" -o "$2"
    else
        youtube-dl --extract-audio --audio-format mp3 "$1" -o ~/Music/Tamil/temp.mp3
    fi
fi

