" Source vim-go
call plug#begin()
Plug 'fatih/vim-go'
Plug 'fatih/molokai'
Plug 'AndrewRadev/splitjoin.vim'
Plug 'SirVer/ultisnips'
Plug 'ctrlpvim/ctrlp.vim'
Plug 'junegunn/goyo.vim'
Plug 'arcticicestudio/nord-vim'
call plug#end()

" Pathogen settings
execute pathogen#infect()

""""""""""""""""""""""
"      Settings      "
""""""""""""""""""""""
set nocompatible                " Enables us Vim specific features
filetype off                    " Reset filetype detection first ...
filetype plugin indent on       " ... and enable filetype detection
set ttyfast                     " Indicate fast terminal conn for faster redraw
set ttymouse=xterm2             " Indicate terminal type for mouse codes
set mouse=a                     " mouse use
set ttyscroll=3                 " Speedup scrolling
set laststatus=2                " Show status line always
set encoding=utf-8              " Set default encoding to UTF-8
set autoread                    " Automatically read changed files
set autoindent                  " Enable Autoindent
set backspace=indent,eol,start  " Makes backspace key more powerful.
set incsearch                   " Shows the match while typing
set hlsearch                    " Highlight found searches
set noerrorbells                " No beeps
set number                      " Show line numbers
set showcmd                     " Show me what I'm typing
set noswapfile                  " Don't use swapfile
set nobackup                    " Don't create annoying backup files
set splitright                  " Vertical windows should be split to right
set splitbelow                  " Horizontal windows should split to bottom
set autowrite                   " Automatically save before :next, :make etc.
set hidden                      " Buffer should still exist if window is closed
set fileformats=unix,dos,mac    " Prefer Unix over Windows over OS 9 formats
set noshowmatch                 " Do not show matching brackets by flickering
set noshowmode                  " We show the mode with airline or lightline
set ignorecase                  " Search case insensitive...
set smartcase                   " ... but not it begins with upper case
set completeopt=menu,menuone    " Show popup menu, even if there is one entry
set pumheight=10                " Completion window max size
set nocursorcolumn              " Do not highlight column (speeds up highlighting)
set nocursorline                " Do not highlight cursor (speeds up highlighting)
set lazyredraw                  " Wait to redraw

set linebreak                   " Break lines at word
set showbreak=+++               " Wrap-broken line prefix
set showmatch                   " Show matching brace
set cursorline                  " Set cursor line
set history=10000               " history

" indentation
set smartindent                 " autoindent for the next level
set smarttab                    " Enable smart tabs

"autocmd FileType go setlocal shiftwidth=8 tabstop=8 softtabstop=8 noexpandtab
"autocmd FileType c,cpp,h,hpp setlocal shiftwidth=3 tabstop=3 softtabstop=3 expandtab

" Displays '-' for trailing space, '>-' for tabs and '_' for
" non breakable space
"set listchars=tab:>-,trail:-,nbsp:_
set listchars=trail:-,nbsp:_

" Multi-level hierarchy ctags
set tags=tags;

" TODO: Enable to copy to clipboard for operations like yank, delete, change and put

" TODO Persistent undo not working for now
" This enables us to undo files even if you exit Vim. 
set undofile
" Sets the directory in which file history will be stored
set undodir=~/.vim/undodir

" Colorscheme
syntax enable
set t_Co=256
let g:rehash256 = 1
let g:molokai_original = 1
colorscheme molokai

""""""""""""""""""""""
"      Mappings      "
""""""""""""""""""""""

" Set leader shortcut to a comma ','. By default it's the backslash
let mapleader = ","

" Jump to next error with Ctrl-n and previous error with Ctrl-m. Close the
" quickfix window with <leader>a
map <C-n> :cnext<CR>
map <C-m> :cprevious<CR>
nnoremap <leader>a :cclose<CR>

" Visual linewise up and down by default (and use gj gk to go quicker)
noremap <Up> gk
noremap <Down> gj
noremap j gj
noremap k gk

" Search mappings: These will make it so that going to the next one in a
" search will center on the line it's found in.
nnoremap n nzzzv
nnoremap N Nzzzv

" Act like D and C
nnoremap Y y$

" Enter automatically into the files directory
"autocmd BufEnter * silent! lcd %:p:h
" Start NERDTree
autocmd VimEnter * NERDTree
" Put control on right of NERDTree
autocmd VimEnter * wincmd p

"""""""""""""""""""""
" Enable spell check
" To run this locally, use 'set spell' and 'set spell&' to set and unset this option
"""""""""""""""""""""
set spelllang=en
"set spell         "Enable spell check globally

""""""""""""""""""""""""""""""""""""""""""""
"      Plugin specific configurations      "
""""""""""""""""""""""""""""""""""""""""""""

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Go specific configurations:
" vim-go
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
let g:go_fmt_command = "goimports"
let g:go_autodetect_gopath = 1
let g:go_list_type = "quickfix"

let g:go_highlight_types = 1
let g:go_highlight_fields = 1
let g:go_highlight_functions = 1
let g:go_highlight_function_calls = 1
let g:go_highlight_extra_types = 1
let g:go_highlight_generate_tags = 1

" Open :GoDeclsDir with ctrl-g
nmap <C-g> :GoDeclsDir<cr>
imap <C-g> <esc>:<C-u>GoDeclsDir<cr>


augroup go
  autocmd!

  " Show by default 4 spaces for a tab
  "autocmd BufNewFile,BufRead *.go setlocal noexpandtab tabstop=4 shiftwidth=4

  " :GoBuild and :GoTestCompile
  autocmd FileType go nmap <leader>b :<C-u>call <SID>build_go_files()<CR>

  " :GoTest
  autocmd FileType go nmap <leader>t  <Plug>(go-test)

  " :GoRun
  autocmd FileType go nmap <leader>r  <Plug>(go-run)

  " :GoDoc
  autocmd FileType go nmap <Leader>d <Plug>(go-doc)

  " :GoCoverageToggle
  autocmd FileType go nmap <Leader>c <Plug>(go-coverage-toggle)

  " :GoInfo
  autocmd FileType go nmap <Leader>i <Plug>(go-info)

  " :GoMetaLinter
  autocmd FileType go nmap <Leader>l <Plug>(go-metalinter)

  " :GoDef but opens in a vertical split
  autocmd FileType go nmap <Leader>v <Plug>(go-def-vertical)
  " :GoDef but opens in a horizontal split
  autocmd FileType go nmap <Leader>s <Plug>(go-def-split)

  " :GoAlternate  commands :A, :AV, :AS and :AT
  autocmd Filetype go command! -bang A call go#alternate#Switch(<bang>0, 'edit')
  autocmd Filetype go command! -bang AV call go#alternate#Switch(<bang>0, 'vsplit')
  autocmd Filetype go command! -bang AS call go#alternate#Switch(<bang>0, 'split')
  autocmd Filetype go command! -bang AT call go#alternate#Switch(<bang>0, 'tabe')
augroup END

" build_go_files is a custom function that builds or compiles the test file.
" It calls :GoBuild if its a Go file, or :GoTestCompile if it's a test file
function! s:build_go_files()
  let l:file = expand('%')
  if l:file =~# '^\f\+_test\.go$'
    call go#test#Test(0, 1)
  elseif l:file =~# '^\f\+\.go$'
    call go#cmd#Build(0)
  endif
endfunction


" URL: http://vim.wikia.com/wiki/Highlight_long_lines
" Alternatively, only show the color column when the limit is exceeded
au BufWinEnter *.c,*.h,*.py,*.go,*.cpp,*.hpp let w:m2=matchadd('ErrorMsg','\%>89v.\+', -1)

" Jump to the last position when reopening a file
if has("autocmd")
  au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif
endif

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Pandoc: https://pandoc.org/
" Open doc/docx/rtf/odp/odt files using Vim
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
autocmd BufReadPost *.doc,*.docx,*.rtf,*.odp,*.odt silent %!pandoc "%" -tplain -o /dev/stdout


"""""""""""""""""""""
" Cscope settings
"""""""""""""""""""""
if has('cscope')
  "set cscopetag cscopeverbose
  set cscopetag

  " Un-comment to have simplified support of cscope
  " (WARNING: will not keep track of ctrl-T stack and does not display
  " the list containing all searches to a symbol)
  "if has('quickfix')
  "  set cscopequickfix=s-,c-,d-,i-,t-,e-
  "endif

  cnoreabbrev csa cs add
  cnoreabbrev csf cs find
  cnoreabbrev csk cs kill
  cnoreabbrev csr cs reset
  cnoreabbrev css cs show
  cnoreabbrev csh cs help

  command -nargs=0 Cscope cs add $VIMSRC/src/cscope.out $VIMSRC/src
endif

" Customize the settings for vim-template plugin
" let g:email = "jagannathanr@vmware.com"
" let g:user = "Rohith Jagannathan"
" let g:license = "Desired License"


"============================ Auto complete ==================================

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Autocomplete without plugins:
" Press ctrl+x in INSERT mode (after first few letters if needed):
" * ctrl+p to use the previously used text
" * ctrl+n to use the text that follows this line in file
" * ctrl+f to output name of file
" * ctrl+o to complete a line
" * What set of texts are available to select is based on
"   ':set complete' list
"   This is by default: .,w,b,u,t,i,kspell
"   where . is current file,
"         w is other windows (like split)
"         b is buffers
"         u is unloaded buffers (open and not active)
"         t is tags file
"         i is included files (like #include files in C or imports in python)
"         kspell is look in spelling dictionary when spelling is turned on
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" YouCompleteMe: Code completion tool
"                Support for multiple languages.
"                Currently enabled c-based languages (C, C++), golang and
"                python
" URL: https://github.com/Valloric/YouCompleteMe
" CURRENTLY NOT INSTALLED
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Auto-complete window goes away when you’re done with it
"let g:ycm_autoclose_preview_window_after_completion=1
"map <leader>g  :YcmCompleter GoToDefinitionElseDeclaration<CR>


"============================ Filesystem =====================================

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" NERDTREE: Directory display
" URL: https://github.com/scrooloose/nerdtree
" Description:
"    - Auto start NERDTREE with autocmd above
"    - Use arrow keys to navigate NERDTREE windows
"    - Use g+o to open the file but not move the control to the opened file
"      for quick navigation over the files
"    - Use Ctrl+6 to jump back and forth between 2 files
"    - Use ctrl+f to open the tree to the currently open file
" Alternative: https://github.com/tpope/vim-vinegar
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Map NERDTREE to ctrl+n
map <C-n> :NERDTreeToggle<CR>
" Map NERDTreeFind to ctrl+f. This opens nerdtree to file path
map <C-f> :NERDTreeFind<CR>


""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Ctrl-p: Full path recursive file searcher
" URL: https://github.com/ctrlpvim/ctrlp.vim
" Description:
"     - Press Ctrl+P to start fuzzy search
"       Start typing to do fuzzy and partial search
"     - Supports search using file names, regex, most recently used (MRU)
"       monitoring using buffers modes.
"     - Press <ctrl-f> and <ctrl-b> to cycle between modes described above.
"     - Press <ctrl-r> to switch to regexp mode.
"
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Set max number of files to infinity
let g:ctrlp_max_files=0
" Set max depth in hierarchy to search to a large value
let g:ctrlp_max_depth=50
" Keep the cache file after closing the vim session
let g:ctrlp_clear_cache_on_exit = 0

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Finding files: Fuzzy file finding
" URL: https://www.youtube.com/watch?v=XA2WjJbmmoM
" Topic: How to Do 90% of What Plugins Do (With Just Vim)
" Description: Find files without using plugins
"    - Hit tab to :find for partial match
"    - Use * to make it fuzzy
"    - :b lets you autocomplete any open buffer
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Search down into subfolders
" Provides tab-completion for all file-realted tasks
set path+=**

" Display all matching files when we tab complete.
" Used to list all the file names that we can use tab to navigate and hit
" enter to select.
set wildmenu


"============================ Git settings ===================================

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Git wrapper for vim
" Fugitive: https://github.com/tpope/vim-fugitive
" TODO: Test with vim
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""


"============================== Comments =====================================

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" NERDCommenter: Comment functions
" URL: https://github.com/scrooloose/nerdcommenter
" Description: Comment functions in multiple languages
"     - [count]\cc |NERDComComment|
"       Comment out the current line or text selected in visual mode.
"     - [count]\c<space> |NERDComToggleComment|
"       Toggles the comment state of the selected line(s). If the topmost
"       selected line is commented, all selected lines are uncommented and
"       vice versa.
"     - [count]\cm |NERDComMinimalComment|
"        Comments the given lines using only one set of multipart delimiters.
"     - [count]\cs |NERDComSexyComment|
"       Comments out the selected lines ``sexily'
"     - \c$ |NERDComEOLComment|
"       Comments the current line from the cursor to the end of line.
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""


"============================ Tag settings ===================================

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Tagbar: A class outline viewer for Vim
" URL: https://github.com/majutsushi/tagbar
" Description: Remap tagbar with F8 as shortcut
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
nmap <F8> :TagbarToggle<CR>


"============================ Misc settings ==================================

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" vim-airline: Smart statusline at the bottom of each window
" URL: https://github.com/vim-airline/vim-airline
" Description: Provides following features:
"       - Add smart status line
"       - Smart tab line with below:
"         let g:airline#extensions#tabline#enabled = 1
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"let g:airline#extensions#tabline#enabled = 1
"let g:airline#extensions#tabline#left_sep = ' >'
"let g:airline#extensions#tabline#formatter = 'default'


""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Ack: Search tool for vim
" URL: https://github.com/mileszs/ack.vim
" Description: Search tool that uses ack tool inside vim. Search results
" are seen on a seperate split window.
"     - Install ack (>=2.0)
"       sudo apt-get install ack-grep
"     - Usage: :Ack [options] {pattern} [{directories}]
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""


""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Tabular: Line up text
" URL: https://github.com/godlygeek/tabular
" Description: Line up text to a particular pattern
"     - Enable Visual mode and select text
"       :Tab <pattern>
"       Eg: :Tab /:
"     - If stacking the colons in a column is not desired, run
"       :Tab /:\zs
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"if exists(":Tabularize")
"   nmap \a= :Tabularize /=<CR>
"   vmap \a= :Tabularize /=<CR>
"   nmap \a: :Tabularize /:\zs<CR>
"   vmap \a: :Tabularize /:\zs<CR>
"endif


""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Fold: Fold text
" Description:
"       zf#j creates a fold from the cursor down # lines.
"       zf/ string creates a fold from the cursor to string .
"       zj moves the cursor to the next fold.
"       zk moves the cursor to the previous fold.
"       za toggle a fold at the cursor.
"       zo opens a fold at the cursor.
"       zO opens all folds at the cursor.
"       zc closes a fold under cursor. 
"       zm increases the foldlevel by one.
"       zM closes all open folds.
"       zr decreases the foldlevel by one.
"       zR decreases the foldlevel to zero -- all folds will be open.
"       zd deletes the fold at the cursor.
"       zE deletes all folds.
"       [z move to start of open fold.
"       ]z move to end of open fold.
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"set foldmethod=indent
"set foldlevel=99


""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" SimpylFold: Easy fold mainly for python
" URL: https://github.com/tmhedberg/SimpylFold
" Description:
"   (same commands as 'code folding' above)
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
let g:SimpylFold_docstring_preview = 1


""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Zoom in/out of windows
" URL: https://github.com/vim-scripts/ZoomWin
" Description: Press <ctrl-w>o:
"  * The current window zooms into a full screen
"  * Again: the previous set of windows is restored
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""


""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Golang support for vim
" URL: https://github.com/fatih/vim-go
" Description: TODO
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

"================================ TODO =======================================

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Syntastic: https://github.com/vim-syntastic/syntastic
"            Syntax checking
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Supertab : https://github.com/ervandew/supertab
"            Tab completion
" vim-pasta: https://github.com/sickill/vim-pasta
"            Pasting with indentation
"
" vim-flake8: https://github.com/nvie/vim-flake8
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Learn:
" Disable arrow keys and simple movements: https://github.com/takac/vim-hardtime
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" Ignore version control directories
"set wildignore+=*/.git/*,*/.hg/*,*/.svn/*
"let g:ctrlp_custom_ignore = {
"  \ 'file': '\v(\.cpp|\.h|\.hh|\.c|\.java|\.py|\.sh|\.sc)@<!$'
"  \ }

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Visual mode: v
" Description:
" - Re-format lines in a file:
"   :set textwidth=80
"   Press gq to reformat the paragraph the cursor is currently highlighting
"   Press gqG to reformat entire file
"
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Change tab and width for python files which are overwritten by jedi-vim
" This is called from ~/.vim/after/ftplugin/python.vim.
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
function! SetupPython()
    " Here, you can have the final say on what is set.  So
    " fixup any settings you don't like.
    setlocal softtabstop=3
    setlocal tabstop=3
    setlocal shiftwidth=3
endfunction
command! -bar SetupPython call SetupPython()


""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Write mode: Write mode for a non-code vim session
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
:command Writemode colorscheme nord | setlocal spell | Goyo 70

