" use vim improvements
set nocompatible

set number                      " Show line numbers
set linebreak                   " Break lines at word
set showbreak=+++               " Wrap-broken line prefix
set showmatch                   " Show matching brace
"set spell                       " Enable spell check
set ignorecase                  " case (in)sensitive search
set smartcase                   " smart search
set hlsearch                    " highlight searches
set splitright			" New vertical split is on right side

" 80 character formatting with/without line wrap
set textwidth=0
set formatoptions-=t
set wrapmargin=0
" following two lines highlights in blue if over 80 characters
"hi OverLength ctermbg=darkblue ctermfg=white
"match OverLength /\%90v.\+/

" cursor line
set cursorline
"set ruler                       " cursor pos (row,col) info
"highlight Cursorline cterm=bold
"hi CursorLine term=bold cterm=bold guibg=Grey40
"highlight CursorLine cterm=NONE ctermbg=darkred ctermfg=white
highlight CursorLine cterm=NONE ctermbg=darkgrey

" indentation
set smartindent                 " autoindent for the next level
set shiftwidth=3                " spaces to (auto)indent
set softtabstop=3               " spaces for tab
set expandtab                   " tabs to spaces
set smarttab                    " Enable smart tabs

" other stuff
set ls=2                        " show status (even with one window)
hi StatusLine ctermfg=darkgray  " default status line with filename
set backspace=indent,eol,start  " backspace fixits
set mouse=a                     " mouse use
set history=10000               " history

" Jump to the last position when reopening a file
if has("autocmd")
  au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif
endif

" Multi-level hierarchy ctags
set tags=tags;

" Multi level cscop
" CSCOPE_DB=/path/to/cscope.out; export CSCOPE_DB

" Cscope settings
if has('cscope')
  "set cscopetag cscopeverbose
  set cscopetag

  " Un-comment to have simplified support of cscope
  " (WARNING: will not keep track of ctrl-T stack and does not display
  " the list containing all searches to a symbol)
  "if has('quickfix')
  "  set cscopequickfix=s-,c-,d-,i-,t-,e-
  "endif

  cnoreabbrev csa cs add
  cnoreabbrev csf cs find
  cnoreabbrev csk cs kill
  cnoreabbrev csr cs reset
  cnoreabbrev css cs show
  cnoreabbrev csh cs help

  command -nargs=0 Cscope cs add $VIMSRC/src/cscope.out $VIMSRC/src
endif

" Customize the settings for vim-template plugin
let g:email = "jagannathanr@vmware.com"
" let g:user = "Rohith Jagannathan"
" let g:license = "Desired License"

" Pathogen settings
execute pathogen#infect()
syntax on
filetype plugin indent on

autocmd VimEnter * NERDTree
autocmd VimEnter * wincmd p

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Finding files: Fuzzy file finding
" URL: https://www.youtube.com/watch?v=XA2WjJbmmoM
" Topic: How to Do 90% of What Plugins Do (With Just Vim)
" Description: Find files without using plugins
"    - Hit tab to :find for partial match
"    - Use * to make it fuzzy
"    - :b lets you autocomplete any open buffer
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Search down into subfolders
" Provides tab-completion for all file-realted tasks
set path+=**

" Display all matching files when we tab complete.
" Used to list all the file names that we can use tab to navigate and hit
" enter to select.
set wildmenu

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" vim-airline: Smart statusline at the bottom of each window
" URL: https://github.com/vim-airline/vim-airline
" Description: Provides following features:
"       - Add smart status line
"       - Smart tab line with below:
"         let g:airline#extensions#tabline#enabled = 1
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"let g:airline#extensions#tabline#enabled = 1
"let g:airline#extensions#tabline#left_sep = ' >'
"let g:airline#extensions#tabline#formatter = 'default'


""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Tagbar: A class outline viewer for Vim
" URL: https://github.com/majutsushi/tagbar
" Description: Remap tagbar with F8 as shortcut
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
nmap <F8> :TagbarToggle<CR>
